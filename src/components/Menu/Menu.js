import React from 'react';
import './Menu.scss';
import { Link } from 'react-router-dom';

function Menu(props) {
  return (
    <div className="menu">
      <div className="container">
        <div className="d-flex justify-content-between">
          <h1 className="app-name">{props.title}</h1>
          <ul className="nav">
            <li className="nav-item">
              <Link className="nav-link" to="/">
                Home
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/contact">
                Contact
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Menu;
