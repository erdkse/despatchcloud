import React, { Component } from 'react';
import axios from 'axios';
import './Home.scss';
import logo from './../../logo.svg';
import Menu from '../Menu/Menu';
import Message from './../Message/Message';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      response: null
    };
    this.handleChange = this.handleChange.bind(this);
    this.checkVAT = this.checkVAT.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  checkVAT(event) {
    if (this.state.value) {
      axios
        .get(
          'https://vat.anilcancakir.com/api/vat/check?vat_number=' +
            this.state.value
        )
        .then(response => {
          this.setState({ response: response.data });
        })
        .catch(error => {
          this.setState({ response: error.data });
        });
    }

    event.preventDefault();
  }

  render() {
    const response = this.state.response ? { ...this.state.response } : null;
    let message = response ? <Message data={response.data} /> : <p />;
    return (
      <div>
        <Menu title="Vat Checker" />
        <div className="home-container">
          <div className="container">
            <div className="row">
              <div className="col-md-4 col-sm-12 text-wrapper">
                <h2 className="header">Check the Value Added Tax Number</h2>
                <p className="text">
                  Vat Checker allows you to check the validity of VAT number
                  prior to applying the 0% rate when selling goods or services
                  to EU countries.
                </p>
              </div>
              <div className="col-md-8 col-sm-12">
                <img src={logo} alt="Building" />
              </div>
            </div>

            <div className="d-flex justify-content-center search">
              <div>
                <h3 className="title">VAT Checker</h3>
                <form onSubmit={this.checkVAT}>
                  <div className="input-group">
                    <input
                      value={this.state.value}
                      onChange={this.handleChange}
                      type="text"
                      className="form-control"
                      placeholder="IE123456789"
                    />
                    <div className="input-group-append">
                      <input
                        type="submit"
                        value="Check"
                        className="btn btn-outline-secondary btn-search"
                      />
                    </div>
                  </div>
                </form>
                {message}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
