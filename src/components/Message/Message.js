import React from 'react';
import './Message.scss';

function Message(props) {
  if (props.data.valid) {
    return (
      <div className="success-message">
        <div className="header">The Number is valid on the specified date</div>
        <div className="message">
          <b>Trader Name : </b>
          <span>{props.data.trader_name}</span>
        </div>
        <div className="message">
          <b>Trader Address : </b>
          <span>{props.data.trader_address}</span>
        </div>
      </div>
    );
  }
  return (
    <div className="error-message">
      <div className="header">The Number is invalid on the specified date</div>
    </div>
  );
}

export default Message;
