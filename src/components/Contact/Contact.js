import React from 'react';
import './Contact.scss';
import Menu from '../Menu/Menu';

function Contact() {
  return (
    <div>
      <Menu title="Contact" />
      <div className="container">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Aliquam
          nulla facilisi cras fermentum odio eu. Ac orci phasellus egestas
          tellus rutrum tellus pellentesque. Cursus sit amet dictum sit amet
          justo. Risus quis varius quam quisque id diam vel quam elementum.
          Scelerisque felis imperdiet proin fermentum leo vel orci porta non.
          Fringilla est ullamcorper eget nulla facilisi. Blandit libero volutpat
          sed cras ornare arcu dui vivamus. Penatibus et magnis dis parturient
          montes. Eu lobortis elementum nibh tellus. Mattis pellentesque id nibh
          tortor id aliquet lectus proin. Vel facilisis volutpat est velit
          egestas dui id. Sit amet nisl purus in. Morbi non arcu risus quis
          varius. Dignissim cras tincidunt lobortis feugiat vivamus at augue
          eget. Facilisis magna etiam tempor orci eu lobortis.
        </p>

        <p>
          Egestas pretium aenean pharetra magna ac placerat vestibulum lectus
          mauris. Sit amet risus nullam eget felis. Malesuada fames ac turpis
          egestas integer eget aliquet nibh praesent. Et sollicitudin ac orci
          phasellus egestas. Elementum eu facilisis sed odio morbi quis commodo
          odio. A erat nam at lectus urna duis. Netus et malesuada fames ac
          turpis egestas maecenas pharetra. Laoreet id donec ultrices tincidunt
          arcu non sodales neque sodales. Habitasse platea dictumst quisque
          sagittis purus sit. Aliquam malesuada bibendum arcu vitae. Tincidunt
          dui ut ornare lectus sit amet est placerat. Mauris augue neque gravida
          in fermentum et sollicitudin. Vitae purus faucibus ornare suspendisse
          sed nisi lacus sed viverra. Nibh mauris cursus mattis molestie a
          iaculis at erat pellentesque. Orci sagittis eu volutpat odio facilisis
          mauris sit amet massa. Mauris rhoncus aenean vel elit scelerisque
          mauris pellentesque pulvinar pellentesque. Ultricies leo integer
          malesuada nunc vel risus commodo viverra maecenas. Vitae aliquet nec
          ullamcorper sit amet risus nullam eget. Nibh cras pulvinar mattis nunc
          sed blandit libero. Tincidunt ornare massa eget egestas purus viverra.
        </p>

        <p>
          Condimentum mattis pellentesque id nibh tortor id. Eu turpis egestas
          pretium aenean. Enim blandit volutpat maecenas volutpat blandit
          aliquam etiam erat. Elit duis tristique sollicitudin nibh. Adipiscing
          elit duis tristique sollicitudin nibh sit amet. Eget arcu dictum
          varius duis at consectetur lorem donec massa. Malesuada pellentesque
          elit eget gravida cum sociis natoque penatibus. Pretium viverra
          suspendisse potenti nullam ac tortor vitae purus faucibus. Cursus
          turpis massa tincidunt dui ut ornare lectus sit amet. Non sodales
          neque sodales ut etiam. Nunc faucibus a pellentesque sit amet
          porttitor eget dolor morbi. Morbi leo urna molestie at. Commodo
          viverra maecenas accumsan lacus vel facilisis volutpat est. Tortor
          condimentum lacinia quis vel eros donec ac odio. Libero nunc consequat
          interdum varius sit amet. Magna sit amet purus gravida quis. Aliquam
          etiam erat velit scelerisque in dictum.
        </p>

        <p>
          Scelerisque varius morbi enim nunc faucibus a. Lectus urna duis
          convallis convallis tellus id interdum velit. Quis ipsum suspendisse
          ultrices gravida dictum fusce ut placerat. Mattis nunc sed blandit
          libero volutpat sed cras. Ipsum consequat nisl vel pretium lectus quam
          id leo in. Faucibus a pellentesque sit amet. Volutpat commodo sed
          egestas egestas. Ultrices gravida dictum fusce ut. Sit amet commodo
          nulla facilisi nullam vehicula ipsum a arcu. Dolor magna eget est
          lorem ipsum dolor sit amet.
        </p>

        <p>
          Euismod elementum nisi quis eleifend quam adipiscing vitae proin. Nisi
          vitae suscipit tellus mauris a diam maecenas sed enim. Varius vel
          pharetra vel turpis nunc. Vitae et leo duis ut diam quam. Non tellus
          orci ac auctor augue mauris augue neque. Enim nunc faucibus a
          pellentesque sit amet porttitor eget. Ac feugiat sed lectus vestibulum
          mattis ullamcorper velit sed. Morbi tristique senectus et netus et
          malesuada. Enim nunc faucibus a pellentesque sit amet porttitor.
          Maecenas accumsan lacus vel facilisis volutpat est velit egestas dui.
          Dui id ornare arcu odio ut sem nulla pharetra. Consectetur adipiscing
          elit duis tristique sollicitudin nibh sit amet.
        </p>
      </div>
    </div>
  );
}

export default Contact;
