import React from 'react';
import './Header.scss';
import { Link } from 'react-router-dom';

function Header() {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-blue">
        <div className="container">
          <Link className="navbar-brand" to="/">
            <span>support@despatchcloud.com</span>
            <span className="divider" />
            <span>01377 455 180</span>
          </Link>
          <Link className="company-name float-right" to="/">
            Despatch Cloud
          </Link>
        </div>
      </nav>
    </div>
  );
}

export default Header;
