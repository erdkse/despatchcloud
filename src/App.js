import React from 'react';
import './App.scss';
import Header from './components/Header/Header';
import Home from './components/Home/Home';
import Contact from './components/Contact/Contact';
import { BrowserRouter, Route } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <Header />

      <Route path="/" exact component={Home} />
      <Route path="/contact" component={Contact} />
    </BrowserRouter>
  );
}

export default App;
